from django.urls import include, path

from . import views

from story_1 import views as story_1_views

# from story_1 import views as story1_views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('hobby/', views.hobby, name='hobby'),
    path('story1/', story_1_views.index, name="story_1"),
]
