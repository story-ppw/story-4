from django.shortcuts import render


def home(request):
    return render(request, 'home.html')

def hobby(request):
    return render(request, 'hobby.html')
